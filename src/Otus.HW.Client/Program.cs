﻿using IdentityModel.Client;
using Newtonsoft.Json.Linq;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace Client
{
    public class Program
    {
        private static async Task Main()
        {
            Console.WriteLine("Push Enter to start");
            Console.ReadKey();


            // discover endpoints from metadata
            
            var client = new HttpClient();

            var disco = await client.GetDiscoveryDocumentAsync("http://localhost:5050");
            if (disco.IsError)
            {
                Console.WriteLine(disco.Error);
                return;
            }

            // request token
            var tokenResponse = await client.RequestClientCredentialsTokenAsync(new ClientCredentialsTokenRequest
            {
                Address = disco.TokenEndpoint,
                ClientId = "otus.hw.api",
                ClientSecret = "511536EF-F270-4058-80CA-1C89C192F69A",

                Scope = "api"
            });
            
            if (tokenResponse.IsError)
            {
                Console.WriteLine(tokenResponse.Error);
                return;
            }

            Console.WriteLine(tokenResponse.Json);
            Console.WriteLine("\n\n");

            // call api
            var apiClient = new HttpClient();
            apiClient.SetBearerToken(tokenResponse.AccessToken);

            var response = await apiClient.GetAsync("http://localhost:5005/identity");
            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine(response.StatusCode);
            }
            else
            {
                var content = await response.Content.ReadAsStringAsync();
                Console.WriteLine(JArray.Parse(content));
            }

            Console.WriteLine("Auth successfuly. to get weather? (enter if DA)");

            Console.ReadKey();

            var weatherData = await apiClient.GetStringAsync("http://localhost:5005/WeatherForecast");

            Console.WriteLine(weatherData);

            Console.ReadKey();

        }
    }
}